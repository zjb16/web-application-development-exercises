> **NOTE:** This README.md file should eb placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Development

## Zachary Birchall

### Class Number Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Use MYSQL and AMPPS
    - Create Hello, World
    - Write Database Servlet
    - Write client based web application
    - Use git commands
    - Provide screenshots

3. [A3 README.md](a3/README.md "My A3  README.md file")
    - Create Entity Relationship Diagram
    - Provide Screenshot of ERD
    - Provide links to A3 mwb and sql files

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Add form controls to match attributes of customer entity
    - Add jQuery and validation expressions
    - Limit number of characters for each control
    - Provide screenshot of both failed and confirmed validations

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Add Server-side validation to Customer entity inputs
    - Provide screenshots of both failed and passed validations

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Prepared statements to help prevent SQL injection
    - JSTL to prevent XSS
    - Add insert functionality to A4
    - Provide screenshots of valid user form entry, passed validation, and associated database entry

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - MVC Framework, using the basic client-, server-side validation
    - Prepared statements to help prevent SQL injection
    - JSTL to prevent XSS
    - Complete CRUD functionality
    - Provide screenshots