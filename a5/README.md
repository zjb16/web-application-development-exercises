# LIS 4368

## Zachary Birchall

### Assignment 5 Requirements:

1. Create prepared statements to help prevent SQL injection
2. Use JSTL to prevent XSS
4. Adds functionality to A4
5. Create README
6. Assignment Screeshots

#### README.md file should include the following items:

* Screenshots of valid user form entry, passed validation, and associated database



##### Assignment Screenshots:

*Valid User Form Entry*:

![Valid User Form Entry](img/valid.png "Valid User Form Entry")

*Passed Validation*:

![Passed Validation](img/passed_validation.png "Passed Validation")

*Associated Database*
![Associated Database](img/associated_database.png "Associated Database")
