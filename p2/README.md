# LIS 4368

## Zachary Birchall

### Project 2 Requirements:

1. MVC framework, susing basic client-, server-side validation
2. Create prepared statements to help prevent SQL injection
3. Use JSTL to prevent XSS
4. Complete CRUD functionality
5. Create README
6. Assignment Screeshots

#### README.md file should include the following items:

* Screenshots of valid user form entry, passed validation, displayed data, modify form, modified data, delete warning, and associated database changes



##### Assignment Screenshots:

*Valid User Form Entry*:                                       
                                                               
![Valid User Form Entry](img/valid.png "Valid User Form Entry")

*Passed Validation*:

![Passed Validation](img/passed_validation.png "Passed Validation")

*Display Data*:

![Display Data](img/display_data.png "Display Data")

*Modify Form*:

![Modify Form](img/modify_form.png "Modify Data")

*Modified Data*:

![Modified Data](img/modified_data.png "Modified Data")

*Delete Warning*

![Delete Warning](img/delete_warning.png "Delete Warning")

*Associated Database Changes*
![Associated Database Changes](img/associated_database.png "Associated Database Changes")
