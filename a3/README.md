# LIS 4368

## Zachary Birchall

### Assignment 3 Requirements:

1. Entity Relationship Diagram
2. Include data (at least 10 records each table)
3. Create README
4. Assignment Screeshots

#### README.md file should include the following items:

* Screenshot of ERD
* Link to a3.mwb
* Link to a3.sql


##### Assignment Screenshots:

*Screenshot of A3 ERD*:

![A3 ERD](img/a3ERD.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
