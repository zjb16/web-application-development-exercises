# LIS 4368

## Zachary Birchall

### Project 1 Requirements:

1. Add form controls to match attributes of customer entity
2. Add jQuery and validation expressions
3. Create README
4. Assignment Screeshots

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of confirmed validation


##### Assignment Screenshots:

*Screenshot of failed validation*:

![Failed Validation](img/failed_validation.png "Incorrect value entries")

*Screenshot of confirmed validation*:

![Confirmed Validation](img/confirmed_validation.png "Correct value entries")
