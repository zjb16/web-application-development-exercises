# LIS 4368

## Zachary Birchall

### Assignment 4 Requirements:

1. Add Server-side validation to Customer entity inputs
2. Create README
3. Assignment Screeshots

#### README.md file should include the following items:

* Screenshots of failed and passed validations



##### Assignment Screenshots:

*Failed Validation*:

![Failed Validation](img/failed_validation.png "Failed validation")

*Passed Validation*:

![A3 MWB File](img/passed_validation.png "Passed Validation")

