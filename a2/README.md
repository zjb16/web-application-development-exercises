# LIS 4368

## Zachary Birchall

### Assignment 2 Requirements:

1. Data-driven website
2. Push local files to Bitbucket repo
3. Create README

#### README.md file should include the following items:

* http://localhost:9999/hello
* http://localhost:9999/hello/index.html
* http://localhost:9999/hello/sayhello
* http://localhost:9999/hello/querybook.html 
* http://localhost:9999/hello/sayhi

#### Git commands w/ short descriptions:
1. git init - initialize a local Git repository
2. git add - add all new and changed files to staging
3. git commit - commit changes
4. git push - push changes to remote repository
5. git pull - update lcoal changes to the newest repository
6. git status - checks status
7. git branch - list branches


##### Assignment Screenshots:

*Screenshot of query book*:

![Query Book](img/querybook_html.png)

*Screenshot of hello world*:

![Hello, World](img/sayhello.png)

*Screenshot of say hi*:

![Say Hi](img/sayhi.png)

*Screenshot of say hello*:

![hello](img/hello.png)

*Screenshot of query results*

![query results](img/query_results.png)
